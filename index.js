// Cutomer class object 

class Customer {
    constructor(email) {
        this.email = email;
        this.cart = new Cart();
        this.orders = []
    }


    check() {
        this.orders.push(this.cart)
        return this

    }
}

class Product {
    constructor(productName, price) {
        this.productName = productName;
        this.price = price;
        this.isActive = true;

    }

    archiveProduct() {
        this.isActive = false;
        return this

    }


    updatePrice(updatepriceProduct) {
        this.price = updatepriceProduct
        return this
    }

}



class Cart {
    constructor() {
        this.contents = [];
        this.totlAmout = 0;


    }
    // addart method  access fromm customer with the property of cart within instanstiate
    // of Cart class object
    addtoCart(product, quantity) {
        this.contents.push({ product, quantity: quantity })
        return this

    }


    showCartcontents() {
        // console.log(this.contents)
        return this.contents;


    }


    updateProductQuantity(product, quantity) {
        let updateQuantity = this.contents
        for (let index = 0; index < updateQuantity.length; index++) {
            const element = updateQuantity[index];
            console.log(element)
            const update = element.product.productName === product ? element.quantity = quantity : false;
            return this
        }



    }

    clearCartContent() {
        this.contents = [];
        return this

    }

    computeTotal() {
        let total = 0
        let computeCart = this.contents;
        for (let index = 0; index < computeCart.length; index++) {
            const element = computeCart[index];
            // console.log(element.product.price)
            total = element.product.price * element.quantity + total

        }
        this.totlAmout = total
        return this
    }


}

const john = new Customer("jonh@mail.com")


// Product Creation
const prodA = new Product("soap", 99.9)
const prodb = new Product("shampoo", 12)
const prodc = new Product("colgate", 9)

// updatePrice
// prodA.updatePrice()
// prodb.updatePrice()
// prodc.updatePrice()


//archive product

// prodA.archiveProduct()
// prodb.archiveProduct()
// prodc.archiveProduct()


// add to Cart
john.cart.addtoCart(prodA, 4)


// show cart Contents
john.cart.showCartcontents()

// updateQuantity 
// john.cart.updateProductQuantity("soap", 5)

// clearContent 
john.cart.clearCartContent()

// compute the cart

john.cart.computeTotal()